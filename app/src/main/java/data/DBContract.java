package data;

import android.provider.BaseColumns;

/**
 * Created by Админ on 12.03.2017.
 */

public class DBContract {
    private DBContract(){}

    public static final class TableLevels implements BaseColumns {

        public static final String TABLE_NAME = "levels";

        public final static String _ID = BaseColumns._ID;
        public final static String state = "stat";
        public final static String stars = "stars";
        public final static String word = "word";
        public final static String list_of_words = "list_of_words";
        public final static String progress = "progress";

        public static final int stars_zero = 0;
        public static final int stars_one = 1;
        public static final int stars_two = 2;
        public static final int stars_tree = 3;
    }

}
