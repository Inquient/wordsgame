package com.orbita.wordsofword;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Админ on 07.04.2017.
 */

public class LettersAdapter extends BaseAdapter {

    private final Activity activity;
    private char[] letterArray;

    public LettersAdapter(Activity a, String word){
        this.activity = a;
        String s = word + "<";
        this.letterArray = s.toCharArray();

//        HashSet<String> letterSet = new HashSet<>();    //Эта дикая конструкция служит для того,
//        char[] s = word.toCharArray();                  //чтобы избавиться от повторяющихся букв.
//        for(int i=0; i<=word.length();i++){             //Почему HashSet нельзя сделать из массива символов?
//            if(i==word.length()){
//                letterSet.add("<");
//            }
//            else{
//                letterSet.add(String.valueOf(s[i]));
//            }
//        }
//        this.letterArray = letterSet.toArray(new String[letterSet.size()]);
    }

    public int getCount() {
        return this.letterArray.length;
    }

    public Object getItem(int position) {
        return this.letterArray[position];
    }

    public long getItemId(int position) {
        return this.letterArray[position];
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View cellView;
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {
//            cellView = new View(this.activity);
            cellView = inflater.inflate(R.layout.letter, parent, false);

        } else {
            cellView = convertView;
        }

        TextView letter = (TextView) cellView.findViewById(R.id.letter);
        letter.setText(String.valueOf(this.letterArray[position]));

        return cellView;
    }

    public String getLetter(int position){
        return String.valueOf(this.letterArray[position]);
    }
}
