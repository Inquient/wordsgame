package com.orbita.wordsofword;

import android.app.Activity;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import data.DBContract;
import data.DBHelper;

/**
 * Created by Админ on 05.03.2017.
 */

public class LevelsAdapter extends BaseAdapter {

    private final Activity activity;

    private Cursor c = null;
    // references to our images
    private final ArrayList<Level> levelArray;

    private final Bundle HintsBundle;

    public LevelsAdapter(Activity a) {
        this.activity = a;
//        this.levelArray = new ArrayList<Level>();
        DBHelper myDbHelper = new DBHelper(this.activity);
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
//        Toast.makeText(this.activity, "Success", Toast.LENGTH_SHORT).show();
        c = myDbHelper.query("levels", null, null, null, null, null, "LENGTH(list_of_words)");
        int num = 1;

        ArrayList<Level> buffer = new ArrayList<Level>();
        while(c.moveToNext()){
            boolean state;
            if(c.getInt(1) == 1){
                state = true;
            }
            else{
                state = false;
            }
            Level l = new Level(num++, state, c.getInt(2), c.getString(3), c.getString(4), c.getString(5));
            buffer.add(l);
        }
        this.levelArray = buffer;

        c = myDbHelper.query("hints", null, null, null, null, null, null);
        c.moveToNext();
        HintsBundle = createHintsBundle(c.getInt(1),c.getInt(2),c.getInt(3),c.getInt(4));
    }

    @Override
    public int getCount() {
        return levelArray.size();
    }

    @Override
    public Level getItem(int position) {
        return this.levelArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.levelArray.get(position).getNumber();
    }

    public static class ViewHolder {
        public ImageView back;
        public TextView number;
        public ImageView star1;
        public ImageView star2;
        public ImageView star3;
    }

    // create a new ImageView for each item referenced by the Adapter
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.level, parent, false);

            holder = new ViewHolder();
            holder.back = (ImageView) convertView.findViewById(R.id.lvl_back);
            holder.number = (TextView) convertView.findViewById(R.id.number);
            holder.star1 = (ImageView) convertView.findViewById(R.id.star1);
            holder.star2 = (ImageView) convertView.findViewById(R.id.star2);
            holder.star3 = (ImageView) convertView.findViewById(R.id.star3);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Level item = levelArray.get(position);

        if(item != null) {

            if (!item.getOpen()) {
                holder.back.setImageResource(R.drawable.back_for_lvl_close);
                holder.number.setText("");

                holder.star1.setVisibility(View.INVISIBLE);
                holder.star2.setVisibility(View.INVISIBLE);
                holder.star3.setVisibility(View.INVISIBLE);
            } else {
                holder.back.setImageResource(R.drawable.back_for_lvl);
                holder.number.setText(String.valueOf(item.getNumber()));

                int stars = item.getStars();
                switch (stars) {
                    case 0:
                        break;
                    case 1:
                        holder.star1.setVisibility(View.VISIBLE);
                        holder.star2.setVisibility(View.INVISIBLE);
                        holder.star3.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        holder.star1.setVisibility(View.VISIBLE);
                        holder.star2.setVisibility(View.VISIBLE);
                        holder.star3.setVisibility(View.INVISIBLE);
                        break;
                    case 3:
                        holder.star1.setVisibility(View.VISIBLE);
                        holder.star2.setVisibility(View.VISIBLE);
                        holder.star3.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

        return convertView;
    }

    public Bundle getItemBundle(int position) {
        Bundle bundle = new Bundle();

        bundle.putInt("number", this.levelArray.get(position).getNumber());
        bundle.putBoolean("open", this.levelArray.get(position).getOpen());
        bundle.putInt("stars", this.levelArray.get(position).getStars());
        bundle.putString("word", this.levelArray.get(position).getWord());
        bundle.putString("list_of_words", this.levelArray.get(position).getList_of_words_AsString());
        bundle.putString("progress", this.levelArray.get(position).getProgress_AsString());

        return bundle;
    }

    private Bundle createHintsBundle(int hint_length, int hint_letter, int hint_word, int global_stars){
        Bundle bundle = new Bundle();

        bundle.putInt("hint_length", hint_length);
        bundle.putInt("hint_letter", hint_letter);
        bundle.putInt("hint_word", hint_word);
        bundle.putInt("global_stars", global_stars);

        return bundle;
    }

    public Bundle getHintsBundle(){
        return this.HintsBundle;
    }
}
