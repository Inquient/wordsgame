package com.orbita.wordsofword;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

/**
 * Created by Админ on 10.08.2017.
 */

public class AddDialog extends DialogFragment implements DialogInterface.OnClickListener, RewardedVideoAdListener {

    private View form = null;

    private RewardedVideoAd mRewardedAd;

    ImageView watchVideo;

    @Override
    @NonNull
    public Dialog onCreateDialog(final Bundle savedInstanceState){
        form = getActivity().getLayoutInflater().inflate(R.layout.add_dialog, null);

        mRewardedAd = MobileAds.getRewardedVideoAdInstance(getContext());
        mRewardedAd.setRewardedVideoAdListener(this);
        mRewardedAd.loadAd("ca-app-pub-9323060416361289/2807009294", new AdRequest.Builder().build());
//        mRewardedAd.loadAd("ca-app-pub-3940256099942544/5224354917", new AdRequest.Builder().build());

        watchVideo = (ImageView) form.findViewById(R.id.watchVideo);
        watchVideo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mRewardedAd.show();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return(builder.setView(form).create());
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    @Override
    public void onRewarded(RewardItem reward) {
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
    }

    @Override
    public void onRewardedVideoAdClosed() {
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
    }

    @Override
    public void onRewardedVideoAdLoaded() {
    }

    @Override
    public void onRewardedVideoAdOpened() {
    }

    @Override
    public void onRewardedVideoStarted() {
    }
}
