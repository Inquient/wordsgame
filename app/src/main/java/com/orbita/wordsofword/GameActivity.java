package com.orbita.wordsofword;

import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


import java.util.Random;

import data.DBContract;
import data.DBHelper;

public class GameActivity extends AppCompatActivity {

    private AdView mAdView;
//    private InterstitialAd mInterstitialAd;

    Level CurrentLevel;
    Hints HintProfile;

    private String currentHintWord = "";
    private int currentLeterIndex = 1;

    private LettersAdapter ltrAd;
    private EditText new_word;
    private TextView gamefield;
    private TextView proportion;

    private boolean FLAG_SAVE = false;

    ViewGroup starsView;

    TextView length_text;
    TextView letter_text;
    TextView word_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);

        final Bundle CurLvl = getIntent().getBundleExtra("CurrentLevel");
        this.CurrentLevel = new Level(CurLvl.getInt("number"), CurLvl.getBoolean("open"),
                CurLvl.getInt("stars"),CurLvl.getString("word"),CurLvl.getString("list_of_words"),
                CurLvl.getString("progress"));

        final Bundle hints = getIntent().getBundleExtra("HintsBundle");
        this.HintProfile = new Hints(hints.getInt("hint_length"), hints.getInt("hint_letter"),
                hints.getInt("hint_word"), hints.getInt("global_stars"));

        TextView word = (TextView) findViewById(R.id.word);     // Выводим исходное слово данного уровня
        word.setText(this.CurrentLevel.getWord());

        //Загрузка звёзд
        FrameLayout stars = (FrameLayout) findViewById(R.id.stars);
        LayoutInflater inflater = getLayoutInflater();
        starsView = (ViewGroup) inflater.inflate(R.layout.stars, stars, false);
        stars.addView(starsView);
        refreshStars();

        // Загрузка подсказок
        FrameLayout h_len = (FrameLayout) findViewById(R.id.hint_length);
        LayoutInflater inflater1 = getLayoutInflater();
        ViewGroup hint_length = (ViewGroup) inflater1.inflate(R.layout.hint_length, h_len, false);
        h_len.addView(hint_length);
        length_text = (TextView) hint_length.findViewById(R.id.length_text);
        length_text.setText(String.valueOf(HintProfile.hint_length));

        FrameLayout h_let = (FrameLayout) findViewById(R.id.hint_letter);
        LayoutInflater inflater2 = getLayoutInflater();
        ViewGroup hint_letter = (ViewGroup) inflater2.inflate(R.layout.hint_letter, h_let, false);
        h_let.addView(hint_letter);
        letter_text = (TextView) hint_letter.findViewById(R.id.letter_text);
        letter_text.setText(String.valueOf(HintProfile.hint_letter));

        FrameLayout h_wor = (FrameLayout) findViewById(R.id.hint_word);
        LayoutInflater inflater3 = getLayoutInflater();
        ViewGroup hint_word = (ViewGroup) inflater3.inflate(R.layout.hint_word, h_wor, false);
        h_wor.addView(hint_word);
        word_text = (TextView) hint_word.findViewById(R.id.word_text);
        word_text.setText(String.valueOf(HintProfile.hint_word));

        LoadAd();

        // Загрузка клавиатуры
        this.ltrAd = new LettersAdapter(this, this.CurrentLevel.getWord());     // создаём адаптер клавиатуры
        GridView gridView = (GridView) findViewById(R.id.leterGrid);            // и саму клавиатуру
        gridView.setAdapter(ltrAd);
        gridView.setOnItemClickListener(gridviewOnItemClickListener);

        gamefield = (TextView) findViewById(R.id.gamefield);                    // Определяем текстовое поле - игровое поле

        if(CurrentLevel.getProgress_AsString().equals("")){
            gamefield.append(CurrentLevel.getList_of_Caption_AsString());
        }
        else {
            gamefield.setText(CurrentLevel.getProgress_AsString().replaceAll(",", "  "));
            gamefield.append("  ");
            gamefield.append(CurrentLevel.getList_of_Caption_AsString());
        }
        proportion = (TextView) findViewById(R.id.proportion);
        refreshFields();

        new_word = (EditText) findViewById(R.id.new_word);                      // Определяем поле ввода
        new_word.setInputType(InputType.TYPE_NULL);
        new_word.addTextChangedListener(new TextWatcher() {                     // и проверяем что введено
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Если введённое слово есть в списке, то выводим его в игровое поле как отгаданное
                // и вносим в БД новый список отгаданных слов
                String word = new_word.getText().toString();
                if(CurrentLevel.getList_of_words_AsList().contains(word)){
                    if(!CurrentLevel.getProgress_AsList().contains(word)){

                        CurrentLevel.progress.add(word);
                        CurrentLevel.refreshListOfCaption(word);

                        gamefield.setText(CurrentLevel.getProgress_AsString().replaceAll(",", "  "));
                        gamefield.append("  ");
                        gamefield.append(CurrentLevel.getList_of_Caption_AsString());

                        new_word.setText("");
                        refreshFields();

                        float s1 = CurrentLevel.getProgress_AsList().size();
                        float s2 = CurrentLevel.getList_of_words_AsList().size();
                        float prop = s1/s2;

                        if((prop > 0.35) && (CurrentLevel.getStars() == 0)){
                            CurrentLevel.setStars(1);
                            HintProfile.hint_letter += 1;
                            HintProfile.hint_word += 1;
                            HintProfile.global_stars +=1;
                            openVictoryDialog();
                        }
                        if((prop > 0.6) && (CurrentLevel.getStars() == 1)){
                            CurrentLevel.setStars(2);
                            HintProfile.hint_letter += 2;
                            HintProfile.hint_word += 2;
                            HintProfile.global_stars +=1;
                            openVictoryDialog();
                        }
                        if((prop > 0.85) && (CurrentLevel.getStars() == 2)){
                            CurrentLevel.setStars(3);
                            HintProfile.hint_letter += 3;
                            HintProfile.hint_word += 3;
                            HintProfile.global_stars +=1;
                            openVictoryDialog();
                        }

                        refreshStars();
                        letter_text.setText(String.valueOf(HintProfile.hint_letter));
                        word_text.setText(String.valueOf(HintProfile.hint_word));

                        HintProfile.hint_length += word.length();
                        length_text.setText(String.valueOf(HintProfile.hint_length));

                        currentHintWord = "";
                        currentLeterIndex = 1;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        if(!this.FLAG_SAVE){
            updateDB();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.FLAG_SAVE = true;
        saveAndQuit();
    }

    private void updateDB(){
        DBHelper myDbHelper = new DBHelper(this);
        SQLiteDatabase db = myDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.TableLevels.progress, CurrentLevel.getProgress_AsString());
        values.put(DBContract.TableLevels.stars, CurrentLevel.getStars());

        String selection = DBContract.TableLevels._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(CurrentLevel.getNumber()) };

        int count = db.update(
                DBContract.TableLevels.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );

        ContentValues hint_values = new ContentValues();
        hint_values.put("hint_length", HintProfile.hint_length);
        hint_values.put("hint_letter", HintProfile.hint_letter);
        hint_values.put("hint_word", HintProfile.hint_word);
        hint_values.put("global_stars", HintProfile.global_stars);

        String hint_selection = "id LIKE ?";
        String[] hint_selectionArgs = { String.valueOf(1)};

        int hint_count = db.update(
                "hints",
                hint_values,
                hint_selection,
                hint_selectionArgs
        );
    }

    private void refreshFields(){
        String propText = String.valueOf(CurrentLevel.getProgress_AsList().size()) +
                "/" + String.valueOf(CurrentLevel.getList_of_words_AsList().size());
        proportion.setText(propText);
    }

    private void refreshStars(){
        ImageView star1 = (ImageView) starsView.findViewById(R.id.star1);
        ImageView star2 = (ImageView) starsView.findViewById(R.id.star2);
        ImageView star3 = (ImageView) starsView.findViewById(R.id.star3);
        switch(CurrentLevel.getStars()){
            case 1:
                star1.setVisibility(View.VISIBLE);
                break;
            case 2:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                break;
            case 3:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                star3.setVisibility(View.VISIBLE);
                break;
        }
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            if(ltrAd.getLetter(position).equals("<")){
                String s = new_word.getText().toString();
                if(s.length()>0) {
                    new_word.setText(s.substring(0, s.length() - 1));
                }
            }
            else{
                new_word.append(ltrAd.getLetter(position));
            }
        }
    };

    public void home(View v){
        this.FLAG_SAVE = true;
        saveAndQuit();
    }

    public void showWord(View v){
        if(HintProfile.hint_word > 0) {
            Random random = new Random();

            while (true) {
                int index = random.nextInt(CurrentLevel.getList_of_words_AsList().size());
                String word = CurrentLevel.getList_of_words_AsList().get(index);

                if (!CurrentLevel.getProgress_AsList().contains(word)) {
                    new_word.setText(word);
                    break;
                }
            }

            HintProfile.hint_word -= 1;
            word_text.setText(String.valueOf(HintProfile.hint_word));
        }
        else{
            AddDialog ad = new AddDialog();
            ad.show(getSupportFragmentManager(),
                    "vd_dialog");
        }
    }

    public void showLetter(View v){
        if(HintProfile.hint_letter > 0){
            if(currentHintWord.equals("")) {
                currentHintWord = CurrentLevel.getList_of_Caption_AsList().get(0);
                currentLeterIndex = 1;
            }

            String part1 = currentHintWord.substring(0, currentLeterIndex);
            String part2 = currentHintWord.substring(currentLeterIndex).replaceAll("[а-яa-z]", ".");
            currentLeterIndex += 1;

            gamefield.setText(CurrentLevel.getProgress_AsString().replaceAll(",", "  "));
            gamefield.append("  ");
            gamefield.append(part1+part2);
            gamefield.append("  ");
            gamefield.append(CurrentLevel.getList_of_Caption_AsString());

            new_word.setText(part1);

            HintProfile.hint_letter -= 1;
            letter_text.setText(String.valueOf(HintProfile.hint_letter));
        }
        else{
            AddDialog ad = new AddDialog();
            ad.show(getSupportFragmentManager(),
                    "vd_dialog");
        }
    }

    public void openVictoryDialog(){
        Bundle bundle = new Bundle();
        bundle.putInt("stars", CurrentLevel.getStars());
        bundle.putInt("hint_words", HintProfile.hint_word);

        VictoryDialog vd = new VictoryDialog();
        vd.setArguments(bundle);
//        vd.setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        vd.show(getSupportFragmentManager(),
                "vd_dialog");

    }

    public void saveAndQuit(){
        updateDB();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("ShowAd", true);
        startActivity(intent);
    }

    private void LoadAd(){
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setVisibility(View.VISIBLE);
    }
}
