package com.orbita.wordsofword;
//qwer_0247_rewq
//key0

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import data.DBContract;
import data.DBHelper;

public class MainActivity extends AppCompatActivity {

    Cursor c = null;
    private LevelsAdapter lvlAd;

    private InterstitialAd mInterstitialAd;
//    private RewardedVideoAd mRewardedAd;

    private boolean ShowAd = false;

    Hints HintProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.lvlAd = new LevelsAdapter(this);

        final Bundle hints = lvlAd.getHintsBundle();
        this.HintProfile = new Hints(hints.getInt("hint_length"), hints.getInt("hint_letter"),
                hints.getInt("hint_word"), hints.getInt("global_stars"));

        LoadInterstitialAd();
//        LoadRewardedVideoAd();
        ShowAd = getIntent().hasExtra("ShowAd");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if(ShowAd) {
                    mInterstitialAd.show();
                }
            }
        });
    }

    @Override
    protected void onStop(){
        super.onStop();
        UpdateLevelsProgress();
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            if(lvlAd.getItem(position).getOpen()){
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra("CurrentLevel", lvlAd.getItemBundle(position));
                intent.putExtra("HintsBundle", lvlAd.getHintsBundle());
                startActivity(intent);
            }
        }
    };

    public void play(View v){
        this.setContentView(R.layout.levels_layout);

        GridView gridview = (GridView) findViewById(R.id.levelGrid);
        gridview.setAdapter(lvlAd);

        gridview.setOnItemClickListener(gridviewOnItemClickListener);
    }

    public void home(View v){
        this.setContentView(R.layout.activity_main);
    }

    public void sound(View v){

        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        ImageView sound = (ImageView) findViewById(R.id.sound);

        if(sound.getBackground() == ContextCompat.getDrawable(this, R.drawable.btn_sound_on)){
            //turn ringer silent
            am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            sound.setBackground(ContextCompat.getDrawable(this, R.drawable.btn_sound_off));
//            //turn off sound, disable notifications
//            am.setStreamMute(AudioManager.STREAM_SYSTEM, true);
//            //notifications
//            am.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
//            //alarm
//            am.setStreamMute(AudioManager.STREAM_ALARM, true);
//            //ringer
//            am.setStreamMute(AudioManager.STREAM_RING, true);
//            //media
//            am.setStreamMute(AudioManager.STREAM_MUSIC, true);
        }
        else{
            //turn ringer normal
            am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            sound.setBackground(ContextCompat.getDrawable(this, R.drawable.btn_sound_on));
//            // turn on sound, enable notifications
//            am.setStreamMute(AudioManager.STREAM_SYSTEM, false);
//            //notifications
//            am.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
//            //alarm
//            am.setStreamMute(AudioManager.STREAM_ALARM, false);
//            //ringer
//            am.setStreamMute(AudioManager.STREAM_RING, false);
//            //media
//            am.setStreamMute(AudioManager.STREAM_MUSIC, false);
        }
    }

    public void LoadInterstitialAd(){
        MobileAds.initialize(this, "ca-app-pub-9323060416361289~7376809699");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-9323060416361289/1330276091");
//        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

//    public void LoadRewardedVideoAd(){
//        mRewardedAd = MobileAds.getRewardedVideoAdInstance(this);
//        mRewardedAd.loadAd("ca-app-pub-9323060416361289/2807009294", new AdRequest.Builder().build());
////        mRewardedAd.loadAd("ca-app-pub-3940256099942544/5224354917", new AdRequest.Builder().build());
//    }
//
//    @Override
//    public void onRewarded(RewardItem reward) {
//        this.HintProfile.hint_letter +=5;
//        this.HintProfile.hint_word += 5;
//        UpdateDB();
//    }
//
//    @Override
//    public void onRewardedVideoAdLeftApplication() {
//    }
//
//    @Override
//    public void onRewardedVideoAdClosed() {
//        Toast.makeText(this, "Подсказки открыты!", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onRewardedVideoAdFailedToLoad(int errorCode) {
//        Toast.makeText(this, "Извините, видео не загружено!", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onRewardedVideoAdLoaded() {
//    }
//
//    @Override
//    public void onRewardedVideoAdOpened() {
//    }
//
//    @Override
//    public void onRewardedVideoStarted() {
//    }
//
//    private void UpdateDB(){
//        DBHelper myDbHelper = new DBHelper(this);
//        SQLiteDatabase db = myDbHelper.getWritableDatabase();
//
//        ContentValues hint_values = new ContentValues();
//        hint_values.put("hint_length", HintProfile.hint_length);
//        hint_values.put("hint_letter", HintProfile.hint_letter);
//        hint_values.put("hint_word", HintProfile.hint_word);
//        hint_values.put("global_stars", HintProfile.global_stars);
//
//        String hint_selection = "id LIKE ?";
//        String[] hint_selectionArgs = { String.valueOf(1)};
//
//        int hint_count = db.update(
//                "hints",
//                hint_values,
//                hint_selection,
//                hint_selectionArgs
//        );
//    }

    private void UpdateLevelsProgress(){
        DBHelper myDbHelper = new DBHelper(this);
        SQLiteDatabase db = myDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.TableLevels.state, 1);

        String selection = DBContract.TableLevels._ID + " LIKE ?";
        String[] selectionArgs;

        if((HintProfile.global_stars >= 2)&&(HintProfile.global_stars < 4)){
            for(int i = 5; i<=7; i++) {
                selectionArgs = new String[]{String.valueOf(i)};
                int count = db.update(
                        DBContract.TableLevels.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
            }
        }
        if((HintProfile.global_stars >= 4)&&(HintProfile.global_stars < 6)){
            for(int i = 8; i<=10; i++) {
                selectionArgs = new String[]{String.valueOf(i)};
                int count = db.update(
                        DBContract.TableLevels.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
            }
        }
        if((HintProfile.global_stars >= 6)&&(HintProfile.global_stars < 8)){
            for(int i = 11; i<=13; i++) {
                selectionArgs = new String[]{String.valueOf(i)};
                int count = db.update(
                        DBContract.TableLevels.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
            }
        }
        if((HintProfile.global_stars >= 8)&&(HintProfile.global_stars < 10)){
            for(int i = 14; i<=16; i++) {
                selectionArgs = new String[]{String.valueOf(i)};
                int count = db.update(
                        DBContract.TableLevels.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
            }
        }
        if((HintProfile.global_stars >= 10)&&(HintProfile.global_stars < 12)){
            for(int i = 17; i<=20; i++) {
                selectionArgs = new String[]{String.valueOf(i)};
                int count = db.update(
                        DBContract.TableLevels.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
            }
        }
    }

//    public void openAddDialog(View view){
//        AddDialog ad = new AddDialog();
//        ad.show(getSupportFragmentManager(),
//                "vd_dialog");
//    }

    @Override
    public void onBackPressed(){
        setContentView(R.layout.activity_main);
    }
}
