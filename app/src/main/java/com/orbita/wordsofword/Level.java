package com.orbita.wordsofword;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Админ on 05.03.2017.
 */

public class Level {
    private int number;
    public boolean open;
    private int stars = 0;
    private String word;
    private List<String> list_of_words;
    public List<String> progress;
    public List<String> list_of_caption;

    Level(int number, boolean open, int stars, String word, String list_of_words, String progress){
        this.number = number;
        this.open = open;
        this.stars = stars;
        this.word = word;
        // Присваивание значение спискам построенно таким образом потому, что конструкция
        // Arrays.asList(list_of_words.split(",")) - создаёт фиксированный список, который
        // нельзя менять
        this.list_of_words = new ArrayList<String>(Arrays.asList(list_of_words.split(",")));
        if(progress != null){
            this.progress = new ArrayList<String>(Arrays.asList(progress.split(",")));
        }
        else{
            this.progress = new ArrayList<String>();
        }
        CreateListOfCaption();
    }

    private void CreateListOfCaption(){
        if(progress != null){
            list_of_caption = new ArrayList<String>();
            for (String s: list_of_words) {
                if(!progress.contains(s)){
                    list_of_caption.add(s);
                }
            }
        }
        else{
            list_of_caption = list_of_words;
        }
    }

    void refreshListOfCaption(String word){
        list_of_caption.remove(word);
    }

    int getNumber(){
        return number;
    }

    boolean getOpen(){
        return open;
    }

    int getStars(){
        return stars;
    }

    void setStars(int stars){
        this.stars = stars;
    }

    String getWord(){
        return word;
    }

    String getList_of_words_AsString(){
        return TextUtils.join(",", list_of_words);
    }

    String getProgress_AsString(){
        if(progress != null){
            return TextUtils.join(",", progress);
        }
        else{
            return "";
        }
    }

    String getList_of_Caption_AsString(){
        return TextUtils.join("  ", list_of_caption).replaceAll("[а-яa-z]", ".");
    }

    List<String> getList_of_words_AsList(){
        return list_of_words;
    }

    List<String> getProgress_AsList(){
        return progress;
    }

    List<String> getList_of_Caption_AsList(){
        return list_of_caption;
    }
}
