package com.orbita.wordsofword;

/**
 * Created by Админ on 19.06.2017.
 */

public class Hints {
    int hint_length;
    int hint_letter;
    int hint_word;
    int global_stars;

    Hints(int length, int letter, int word, int global_stars){
        this.hint_length = length;
        this.hint_letter = letter;
        this.hint_word = word;
        this.global_stars = global_stars;
    }
}
