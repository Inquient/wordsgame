package com.orbita.wordsofword;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Админ on 28.06.2017.
 */

public class VictoryDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private View form = null;

    Button repeat;
    Button exit;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreateDialog(savedInstanceState);
//        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialog);
        form = getActivity().getLayoutInflater().inflate(R.layout.victory, null);

//        form.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

//        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);

        repeat = (Button) form.findViewById(R.id.repeat);
        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        exit = (Button) form.findViewById(R.id.nextlvl);
        exit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        Bundle bundle = this.getArguments();

        ImageView star1 = (ImageView) form.findViewById(R.id.star1);
        ImageView star2 = (ImageView) form.findViewById(R.id.star2);
        ImageView star3 = (ImageView) form.findViewById(R.id.star3);
        TextView hint_text = (TextView) form.findViewById(R.id.hint_text);
        switch(bundle.getInt("stars")) {
            case 1:
                star1.setVisibility(View.VISIBLE);
                hint_text.setText("+1");
                break;
            case 2:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                hint_text.setText("+2");
                break;
            case 3:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                star3.setVisibility(View.VISIBLE);
                hint_text.setText("+3");
                break;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return(builder.setView(form).create());
    }

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;

        window.setAttributes(windowParams);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }
}
